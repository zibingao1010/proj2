# Individual Project 2: Continuous Delivery of Rust Microservice

## Install Docker
Install docker [Docker](https://docs.docker.com/desktop/install/mac-install/)

## Verify Docker
Use the following command to test whether Docker has been installed well on your system.
```
sudo docker run hello-world
```
If the terminal appeals this means that Docker has been well installed.
```
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
b8dfde127a29: Pull complete
Digest: sha256:308866a43596e83578c7dfa15e27a73011bdd402185a84c5cd7f32a88b501a24
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```

## Rust Microservice Functionality
1. Create a template by
    ```
    cargo new <YOUR PROJECT NAME>
    ```
2. Add corresponding dependencies and bin into `Cargo.toml`. 
3. Add functions in the `src/main.rs` file. For my project, I want to find the opposite value of a given number.
4. Create Dockerfile and add corresponding contents.

## Docker Configuration
1. Build Docker image using `docker build -t <YOUR IMAGE NAME> .`

![image](image1.png)

2. Docker Image created

![image](image2.png)

3. Run `docker run -d -p 8080:8080 <YOUR IMAGE NAME>`, check if the container is created in Docker Desktop.

![image](image3.png)

## To test locally
Build the project. Run the project in the terminal. 
```
cargo build
cargo run
```
Use localhost to see if it works locally.

![image](image4.png)

![image](image5.png)

![image](image6.png)

## CI/CD Pipeline
Enable the CI/CD pipeline by creating the `.gitlab-ci.yml` file.

![image](image7.png)

## Demo Video
[Demo Video](https://gitlab.com/zibingao1010/proj2/-/blob/main/Demo%20Video.mov?ref_type=heads)