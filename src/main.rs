use actix_web::{get, web, App, HttpResponse, HttpServer, Responder};
use serde::Serialize;
use std::collections::HashMap;

#[derive(Serialize)]
struct TripleResponse {
    number: i32,
    opposite: i32,
}

#[get("/")]
async fn index() -> impl Responder {
    "You can use the endpoint /opposite?number=<yournumber> to calculate the opposite value of a number."
}

#[get("/opposite")]
async fn triple(web::Query(info): web::Query<HashMap<String, String>>) -> impl Responder {
    match info.get("number") {
        Some(num_str) => {
            match num_str.parse::<i32>() {
                Ok(number) => {
                    let opposite = number * -1;
                    let response = TripleResponse { number, opposite };
                    HttpResponse::Ok().json(response)
                }
                Err(_) => HttpResponse::BadRequest().body("Invalid number provided"),
            }
        }
        None => HttpResponse::BadRequest().body("Missing number query parameter"),
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(index)
            .service(triple)
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
